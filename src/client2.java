import java.io.*;
import java.net.Socket;
public class client2 {
    public static void main(String[] args) throws IOException {
        //1.建立客户端Socket连接，指定服务器位置和端口
        Socket socket = new Socket("192.168.43.9",8809);
//        Socket socket = new Socket("172.16.43.187",8800);

        //2.得到socket读写流
        OutputStream outputStream = socket.getOutputStream();
        //       PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //3.利用流按照一定的操作，对socket进行读写操作
        String info1 = " 1/4 + 1/6";
        String z= info1;
        String tes="";
        int key = 4;
        for(int j=0;j<z.length( );j++)
        {  char d=z.charAt(j);
            if(d>=0 && d<=127) // 是小写字母
            { d+=key%26;  //移动key%26位
                if(d<0) d+=127;  //向左超界
                if(d>127) d-=127;  //向右超界
            }

            tes+=d;
        }
//        String info = new String(info1.getBytes("GBK"),"utf-8");
        //     printWriter.write(info);
        //     printWriter.flush();
        outputStreamWriter.write(tes);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接收服务器的响应
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("接收服务器的信息密文为：" + reply);
            String s= reply;
            String es="";
            key = -4;
            for(int i=0;i<s.length( );i++)
            {  char c=s.charAt(i);
                if(c>=0 && c<=127) // 是小写字母
                { c+=key%26;  //移动key%26位
                    if(c<0) c+=127;  //向左超界
                    if(c>127) c-=127;  //向右超界
                }

                es+=c;
            }
            System.out.println("接收服务器的信息明文为：" + es);
        }
        //4.关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        //printWriter.close();
        outputStream.close();
        socket.close();
    }
}
