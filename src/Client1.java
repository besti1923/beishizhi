import java.io.*;
import java.net.Socket;

public class Client1 {
    public static void main(String[] args) throws IOException {
        //建立客户端,host为伙伴的IP地址
        Socket socket = new Socket("localhost",8800);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //输出流，获取socket读写流
        OutputStream outputStream = socket.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //利用流按照一定的操作，传递信息，对socket进行读写操作

        //运用凯撒密码，加分！！！
        String s= "( 1 + 2 i ) + ( 1 + -2 i )";    //取命令行参数的第一个字符，即需要加密的明文
        int key=4;         //取命令行参数的第二个字符，即密钥，并将其转为整型
        String es="";      //创建接收密文的字符串变量并初始化
        for(int i=0;i<s.length( );i++){    //逐一对明文字符进行加密
            char c=s.charAt(i);
            if(c>='a' && c<='z') // 是小写字母
            {
                c+=key%26;  //移动key%26位
                if(c<'a') c+=26;  //向左超界
                if(c>'z') c-=26;  //向右超界
            }
            else if(c>='0' && c<='9') // 是数字
            {
                c+=key%10;
                if(c<'0') c+=10;
                if(c>'9') c-=10;
            }
            else if(c>='*' && c<='/')
            {
                c+=key%6;
                if(c<'*') c+=6;
                if(c>'/') c-=6;
            }
            es+=c;
        }

        System.out.println(es);
        String info1 = es;
        outputStreamWriter.write(info1);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接受响应
        String reply = null;
        while (!((reply = bufferedReader.readLine())==null)){
            System.out.println(reply);
        }
        //关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        outputStream.close();
    }
}