//父类，游戏角色
package Lei;

public abstract class Role {
    String name;
    int Hp;

    public void start() {
        System.out.println("创建角色：选择职业");
    }

    public abstract void skill();             //1.抽象方法

    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                ", Hp=" + Hp +
                '}';
    }

    public Role(String name, int hp) {
        this.name = name;
        Hp = hp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return Hp;
    }

    public void setHp(int hp) {
        Hp = hp;
    }
}