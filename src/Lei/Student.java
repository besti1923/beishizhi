package Lei;

public class Student implements Comparable{
    protected String name;
    protected int number;
    protected String hobby;

    protected Student next = null;  //设置一个节点，默认指向空

    public Student(String name, int number, String hobby){
        this.name = name;
        this.number = number;
        this.hobby = hobby;
    }

/*    @Override
    public int compareTo(Object o){
        Student temp = (Student) o;
        if(o == null){
            return  1;
        }else {
            return number > temp.number > 1:-1;
        }
    }*/

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", number=" + number +
                ", hobby='" + hobby + '\'' +
                ", next=" + next +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
