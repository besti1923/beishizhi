package Lei;

public class Chain {
    protected int element;
    protected Chain next = null;

    public Chain(int element) {
        this.element = element;
        this.next = next;
    }

    public int getElement() {
        return element;
    }

    public void setElement(int element) {
        this.element = element;
    }

    public Chain getNext() {
        return next;
    }

    public void setNext(Chain next) {
        this.next = next;
    }
}
