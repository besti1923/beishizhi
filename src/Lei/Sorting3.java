package Lei;

public class Sorting3 {
    private static int a[];
    private static int size;
    private static String list="";

    //希尔排序,正序
    public static <T>
    String shellSort_positive(int[] a){
        int temp = a.length / 2;
        int first, last;
        while (temp > 0){
            for (int i = 0; i + temp <= a.length - 1; i++){
                first = i;
                last = i + temp;
                if (a[first] > a[last]){
                    int temp2 = a[first];
                    a[first] = a[last];
                    a[last] = temp2;
                }
            }
            temp = temp / 2;
        }

        String result = "";
        for (int i = 0; i < a.length;  i++){
            result = result + a[i] + " ";
        }
        return result;
    }

    //逆序
    public static <T>
    String shellSort_inverse(int[] a){
        int temp = a.length / 2;
        int first, last;
        while (temp > 0){
            for (int i = 0; i + temp <= a.length - 1; i++){
                first = i;
                last = i + temp;
                if (a[first] < a[last]){
                    int temp2 = a[first];
                    a[first] = a[last];
                    a[last] = temp2;
                }
            }
            temp = temp / 2;
        }

        String result = "";
        for (int i = 0; i < a.length;  i++){
            result = result + a[i] + " ";
        }
        return result;
    }
}
