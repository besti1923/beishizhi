package com.example.test6_4;                        //实验五同样适用

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.LinkedList;

public class MainActivity6_4 extends AppCompatActivity implements View.OnClickListener{

    private Button b1, b2, b3, b4, b5;
    private TextView t1, t2, t3;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6_4);
        mContext = getApplicationContext();
        b1 = (Button) findViewById(R.id.clean);
        b2 = (Button) findViewById(R.id.read);
        b3 = (Button) findViewById(R.id.enter);
        b4 = (Button) findViewById(R.id.output);
        b5 = (Button) findViewById(R.id.rank);
        t1 = (TextView) findViewById(R.id.link);
        t2 = (TextView) findViewById(R.id.document);
        t3 = (TextView) findViewById(R.id.detail);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        LinkedList<Integer> link = new LinkedList<>();
        String[] strs;
        String[] linkdetail;
        switch (v.getId()) {
            case R.id.clean:
                t1.setText("");
                t2.setText("");
                t3.setText("");
                break;
            case R.id.read:
                File6_4 fHelper = new File6_4(mContext);
                String filename = t2.getText().toString();
                String filedetail = t3.getText().toString();
                try {
                    fHelper.save(filename, filedetail);
                    Toast.makeText(getApplicationContext(), "数据写入成功", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "数据写入失败", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.enter:
                String detail = "";
                File6_4 fHelper2 = new File6_4(getApplicationContext());
                try {
                    String fname = t2.getText().toString();
                    detail = fHelper2.read(fname);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getApplicationContext(), detail, Toast.LENGTH_SHORT).show();
                break;
            case R.id.output:
                String detail1 = "";
                File6_4 fHelper22 = new File6_4(getApplicationContext());
                Intent intent = new Intent(MainActivity6_4.this, Second6_4.class);
                try {
                    String fname = t2.getText().toString();
                    detail1 = fHelper22.read(fname);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                strs = detail1.split(" ");       //文件内容

                String listdetail1 = t1.getText().toString();
                linkdetail = listdetail1.split(" ");

                int i;
                for (i = 0; i < linkdetail.length; i++) {
                    link.add(Integer.valueOf(linkdetail[i]));
                }
                intent.putExtra("date1", "链表：" + link);
                intent.putExtra("date2", "总数：" + link.size());

                link.add(4, Integer.valueOf(strs[0]));

                intent.putExtra("date3", "链表：" + link);
                intent.putExtra("date4", "总数：" + link.size());

                link.addFirst(Integer.valueOf(strs[1]));

                intent.putExtra("date5", "链表：" + link);
                intent.putExtra("date6", "总数：" + link.size());

                link.remove(5);

                intent.putExtra("date7", "链表：" + link);
                intent.putExtra("date8", "总数：" + link.size());

                startActivity(intent);
                break;
            case R.id.rank:
                String detail2 = "";
                File6_4 fHelper4 = new File6_4(getApplicationContext());
                Intent intent1 = new Intent(MainActivity6_4.this, Second6_4.class);
                try {
                    String fname = t2.getText().toString();
                    detail2 = fHelper4.read(fname);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                strs = detail2.split(" ");

                String listdetail2 = t1.getText().toString();
                linkdetail = listdetail2.split(" ");

                for (i = 0; i < linkdetail.length; i++) {
                    link.add(Integer.valueOf(linkdetail[i]));
                }

                link.add(4, Integer.valueOf(strs[0]));
                intent1.putExtra("date1", "链表：" + link);
                intent1.putExtra("date2", "总数：" + link.size());

                link.addFirst(Integer.valueOf(strs[1]));
                intent1.putExtra("date3", "链表：" + link);
                intent1.putExtra("date4", "总数：" + link.size());

                link.remove(5);
                intent1.putExtra("date5", "链表：" + link);
                intent1.putExtra("date6", "总数：" + link.size());

                int temp, tempp, j;
                for (i = 0; i < link.size() - 1; i++) {
                    temp = link.get(i);
                    tempp = i;
                    for (j = i + 1; j < link.size(); j++) {
                        if (link.get(j) > temp) {
                            temp = link.get(j);
                            tempp = j;
                        }
                    }
                    link.add(i, temp);
                    if(tempp != i){
                        link.remove(tempp + 1);
                    }
                    else{
                        link.remove(i + 1);
                    }
                }

                intent1.putExtra("date7", "链表：" + link);
                intent1.putExtra("date8", "总数：" + link.size());

                startActivity(intent1);
                break;
        }
    }
}