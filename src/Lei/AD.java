//子类，角色AD
package Lei;

public class AD extends Role implements Comparable{

    public AD(String name, String args) {
        super(name, Integer.parseInt(args));
    }

    @Override
    public void skill() {                     //重写抽象方法
        System.out.println("魔法水晶箭");
    }

    @Override
    public void start() {
        System.out.println("射手");
    }

    @Override
    public int compareTo(Object o) {          //4.实现两个对象的比较
        AD ad = (AD)o;
        if(ad.getHp() < this.Hp){
            return 1;
        }else{
            return 0;
        }
    }
}