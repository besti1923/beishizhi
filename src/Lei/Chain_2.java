package Lei;

import java.io.*;
import java.util.Scanner;

public class Chain_2 {
    public static Chain head;
    public static int nBeiShizhi = 0;

    public static void main(String[] args) throws IOException {
        Scanner scan=new Scanner(System.in);

        //先输入并创建表头
        System.out.println("Enter numbers("+(nBeiShizhi + 1)+").Type stop if you want to stop.");
        String str = scan.nextLine();
        Chain chain0 = new Chain(Integer.parseInt(str));
        head = chain0;

        //一直输入并连接，直到输入“stop”
        do{
            nBeiShizhi++;
            System.out.println("Enter numbers("+(nBeiShizhi + 1)+").Type stop if you want to stop.");
            str = scan.nextLine();
            if(str.equals("stop")){     //防止整数型链表输入字符串导致错误
                break;
            }
            Chain chain = new Chain(Integer.parseInt(str));
            connect(head, chain);
        }while (!str.equals("stop"));

        //删除
        System.out.println("Delete?(y/n)");
        if (scan.nextLine().equals("y")){
            System.out.println("Please enter the node you want to delete:");
            int node = scan.nextInt();
            delete(head, node);
        }

        //插入
        System.out.println("Insert?(y/n)");
        if(scan.next().equals("y")){                      //此处不可next.Line,否则会读走剩下的链表
            System.out.println("Please enter the location where you want to insert the node:");
            int node = scan.nextInt();
            System.out.println("Please enter an element for this node:");
            int element = scan.nextInt();
            insert(head, element, node);
        }

        //输出
        PrintLinkedList(head);

        //1.文件创建（文件类实例化）
        File file = new File("Z:\\JDK\\FileTest","Chain.txt");
        //2.文件的读写，字节流读写，先写后读
        OutputStream outputStream = new FileOutputStream(file);
        byte[] insert2 = {55,66};                //定义字节流数组
        outputStream.write(insert2);

        InputStream inputStream = new FileInputStream(file);
        int[] num = new int[2];
        int by = 0;
        while (inputStream.available() > 0){                 //读取，用数组存储字节流
            num[by] = inputStream.read();
            by++;
        }
        outputStream.close();
        inputStream.close();                     //结束

        //输出文件内容
        System.out.print("File content is: ");
        for(int i = 0; i < 2; i++) {
            System.out.print(num[i] + " ");
        }
        System.out.print("\n");

        //将文件的第一个数字插入到链表第5位,并输出
        insert(head, num[0], 5);
        PrintLinkedList(head);
        //将文件的第二个数字插入到链表第0位,并输出
        insert(head, num[1], 1);
        PrintLinkedList(head);
        //将文件的第一个数字从链表中删除,并输出
        delete(head, 6);
        PrintLinkedList(head);

        rank();
        PrintLinkedList(head);
    }

    //链表之间的连接，尾插法，遍历
    public static void connect(Chain head, Chain chain2){
        Chain head2 = head;
        while (head2.getNext() != null){
            head2 = head2.getNext();
        }
        head2.setNext(chain2);
    }

    //用来输出的静态方法
    public static void PrintLinkedList(Chain head){
        Chain temp = head;
        String list = "";
        while (temp != null){
            list = list + " " + temp.getElement();
            temp = temp.getNext();
        }
        System.out.println(list);
        System.out.println("Total number of elements:" + nBeiShizhi);
    }

    public static void delete(Chain head, int node){
        Chain temp = head;

        if(node == 1) {                //删除头节点
            Chain_2.head = head.getNext();
        }
        else {                          //遍历至要删除的节点
            for(int i = 1; i < node - 1; i++){
                temp = temp.getNext();
            }
        }

        temp.setNext(temp.getNext().getNext());    //跳过该节点，达到删除的目的
        nBeiShizhi--;                              //总数减1
    }

    public static void insert(Chain head, int element, int node){
        Chain insert = new Chain(element);
        Chain temp = head;

        if(node == 1){
            insert.setNext(head);
            Chain_2.head = insert;
        }
        else {
            for(int i = 0; i < node - 2; i++){
                temp = temp.getNext();
            }
            insert.setNext(temp.getNext());
            temp.setNext(insert);
        }
        nBeiShizhi++;
    }

    public static void rank(){
        Chain temp = Chain_2.head;
        Chain temp1 = Chain_2.head;              //双指针用于排序
        int t , w = 1, e = 1;            //t储存链表当前位置后的最大值，w记录指针位置，e记录排序后重复的数的位置

        for(int i = 1; i < nBeiShizhi; i++){
            t = temp.getElement();
            while (temp1.getNext() != null){
                temp1 = temp1.getNext();
                w++;
                if(temp1.getElement() > t){
                     t = temp1.getElement();
                     e = w + 1;
                }
            }
            insert(head, t, i);        //将最大值放在表头
            delete(head, e);           //删除移动后残留的数
            PrintLinkedList(head);
            w = i + 1;
            if(e <= w) temp = temp.getNext();          //此处遇到困难，问题出在后面的数插到当前位置后，temp已经后移一位,所以要加if
            temp1 = temp;
            if(temp1.getNext() == null) break;         //防止越界
        }
    }
}