package Lei;

public class BinarySearch2 {
    public static <T> int BinarySearch2(int a[], int value, int low, int high) {
        int mid = low + (high - low) / 2;
        if(a[mid] == value)
            return mid;
        else if(a[mid] > value)
            return BinarySearch2(a, value, low, mid - 1);
        else if(a[mid] < value)
            return BinarySearch2(a, value, mid + 1, high);
        else return -1;
    }
}
