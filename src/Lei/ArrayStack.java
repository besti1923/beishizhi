package Lei;

import java.util.Arrays;

public class ArrayStack<T> implements StackADT<T> {  //因为不知道要放什么类型，所以放泛型
    private final static int DEFAULT_CAPACITY = 100; //数组容量

    private int top;
    private T[] stack;  //用数组表示一个栈

    public ArrayStack(){
        this(DEFAULT_CAPACITY); //用this调用自身的数组容量
    }

    public ArrayStack(int initialCapacity){
        top = 0;  //初始化，(指针?)指向底层（总是指向下一次的输入位）
        stack = (T[])(new Object[initialCapacity]); //用Object定义再转化成泛型数组
    }

    @Override
    public void push(T element){     //将数据填充到数组，栈顶+1
        //判断栈是否满
        if(size() == stack.length){
            expandCapacity();  //扩容
        }
        stack[top] = element;
        top++;  //top表示栈的元素个数,指针指向下一个位置
    }

    public T pop() throws EmptyCollectionException{   //此处抛出异常后其他地方可以不用再throws，提高代码复用率
        if(isEmpty()){
            throw new EmptyCollectionException("stack");
        }
        top--;   //先指向当前的栈顶
        T result = stack[top];
        stack[top] = null;  //赋空值
        return result;
    }

    @Override
    public T peek() throws EmptyCollectionException {
        if(isEmpty()){
            throw new EmptyCollectionException("stack");
        }
        return stack[top - 1];  //返回栈顶元素
    }

    @Override
    public boolean isEmpty() {
        if(top == 0){
            return true;
        }
        if(stack[top-1]==null){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public int size() {
        return top;
    }

    private void expandCapacity() {
        stack = Arrays.copyOf(stack,stack.length*2);
    }

    @Override
    public String toString(){
        return Arrays.toString(stack);
    }
}
