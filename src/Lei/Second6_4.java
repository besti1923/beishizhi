package com.example.test6_4;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

package com.example.test6_4;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class Second6_4 extends AppCompatActivity {
    TextView t1, t2, t3, t4, t5, t6, t7, t8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second6_4);
        Intent i = getIntent();//获取用于启动第二个界面的intent
        //调用getStringExtra()，传入对应键值，得到传递的数据
        //由于传的是String，所以对应getStringExtra，int对应getIntExtra，boolean对应getBooleanExtra
        t1 = (TextView)findViewById(R.id.textView1);
        t1.setText(i.getStringExtra("date1"));
        t2 = (TextView)findViewById(R.id.textView2);
        t2.setText(i.getStringExtra("date2"));
        t3 = (TextView)findViewById(R.id.textView3);
        t3.setText(i.getStringExtra("date3"));
        t4 = (TextView)findViewById(R.id.textView4);
        t4.setText(i.getStringExtra("date4"));
        t5 = (TextView)findViewById(R.id.textView5);
        t5.setText(i.getStringExtra("date5"));
        t6 = (TextView)findViewById(R.id.textview6);
        t6.setText(i.getStringExtra("date6"));
        t7 = (TextView)findViewById(R.id.textView7);
        t7.setText(i.getStringExtra("date7"));
        t8 = (TextView)findViewById(R.id.textView8);
        t8.setText(i.getStringExtra("date8"));
    }
}
