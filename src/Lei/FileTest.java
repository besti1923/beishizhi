package Lei;

import java.io.*;

public class FileTest {
    public static void main(String[] args) throws IOException {
        //1.文件创建（文件类实例化）
        File file = new File("Z:\\JDK\\FileTest","Chain.txt");  //此处生成的是对象，不是文件
        //file.mkdirs();                      //创建目录
        try{
            if(!file.exists()){
                file.createNewFile();
            }
        }catch (IOException e){          //如果没有文件夹则报错
            System.out.println(e);
        }
        /*File file1 = new File("Z:\\JDK\\FileTest\\1923\\1923.txt");
        file.mkdirs();
        try{
            if(!file1.exists()){
                file1.createNewFile();
            }
        }catch (IOException e){
            System.out.println(e);
            file1.mkdirs();      //会生成1923和1923.txt两个文件夹
        }
        System.out.println(file.exists());     //检查文件是否存在
        System.out.println(file1.exists());*/
        //file.delete()   删除

        //2.文件读写
        //第一种，字节流读写，先写后读
/*        OutputStream outputStream = new FileOutputStream(file);    //添加异常到方法的节名，会在第6行+throws
        byte[] hello = {'H','e','l','l','o'};          //定义字节流数组
        outputStream.write(hello);             //改变第6行的异常为IO异常；
        outputStream.flush();         //删掉也可以

        InputStream inputStream = new FileInputStream(file);
        while (inputStream.available()>0){      //文件里由内容的时候就读取,字节流每次读一个
            System.out.print((char)inputStream.read()+" ");  //读取文件内容(ASCLL码），强行转换
        }
        outputStream.close();
        inputStream.close();     //结束*/

        //第二种，字符流
        Writer writer = new FileWriter(file, true);    //加参数true才能追加，不然会刷掉
        writer.write("55,66");
        writer.append("123asdfghjkl321");       //附加
        writer.flush();           //flush后才能写

        Reader reader = new FileReader(file);
        System.out.println("下面是用Reader读出的数据：");
        while (reader.ready()){
            System.out.print((char)reader.read()+"");
        }
    }
}
