package Lei;

public class LinkedListExample1923 {
    public static void main(String[] args) {
        Student hao = new Student("haoyixuan",20192302,"打游戏");
        Student bei = new Student("beishizhi",20192316,"学习？？");
        Student guo = new Student("GuoQuanWu",20192325,"看电影！！");
        Student yang = new Student("yanglikai",20192326,"泡妞");

        Student niu = new Student("niuzimeng",20192328,"学Java");   //链形成以后再新加一个,头插法/尾插法

        Student head = hao;   //head为引用头指向hao，开始形成链
        hao.next = bei;    //链表形成方式；
        bei.next = guo;
        guo.next = yang;   //yang为空，结尾

/*        //头插法
        niu.next = head;
        head = niu;*/

        //尾插法,关闭上帝视角,不知道最后一个是谁
        Student point = head;
        while(point.next != null){
            point = point.next;
        }
        point.next = niu;

        PrintLinkedList(head);
    }

    public static void PrintLinkedList(Student Head){   //此方法可以在Student类定义,在此定义的好处是不用加static，主程序可以直接引用
        Student point = Head;    //防止表头丢失，先存一下,可以不用
        while (point != null){   //遍历链表!
            System.out.println("Name: " + point.name + " Number: " + point.number + " hobby: " + point.hobby);
            point = point.next;
        }
    }
}