package Lei;


import java.util.Arrays;

public class Searching3 {

    //顺序查找
    public static <T> boolean linearSearch(T[] date,T target){
        int index;
        date[0] = target;

        for(index = date.length-1; !date[index].equals(target); --index){

        }
        return index == 0 ? false : true;
    }

    //二分查找，递归版本
    //需要升序排列，返回-1表示搜索失败
    public static <T> int BinarySearch2(int a[], int value, int low, int high) {
        int mid = low + (high - low) / 2;
        if(a[mid] == value)
            return mid;
        else if(a[mid] > value)
            return BinarySearch2(a, value, low, mid - 1);
        else if(a[mid] < value)
            return BinarySearch2(a, value, mid + 1, high);
        else return -1;
    }

    //插值查找
    //需要升序排列，返回0表示搜索失败
    public static <T> int InsertionSearch(int a[], int value, int low, int high)
    {
        int mid = low+(value - a[low]) / (a[high] - a[low]) * (high - low);
        if(a[mid] == value)
            return mid;
        else if(a[mid] > value)
            return InsertionSearch(a, value, low, mid-1);
        else if(a[mid] < value)
            return InsertionSearch(a, value, mid+1, high);
        else return 0;
    }

    //斐波那契查找
    //需要升序排列，返回0表示搜索失败
    //先建立斐波那契数组,定义其长度
    public static void Fibonacci(int F[]){
        int maxSize = 20;
        F[0] = 0;
        F[1] = 1;
        for (int i = 2; i < maxSize; i++){
            F[i] = F[i - 1] + F[i - 2];
        }
    }

    //斐波那契查找方法
    public static <T> int fibonacciSearching(int a[], int target, int size){
        int low = 0;
        int high = size - 1;
        int maxSize = 20;

        //创建斐波那契数组
        int F[] = new int[maxSize];
        Fibonacci(F);

        //计算n位于斐波那契数列的位置
        int k = 0;
        while (size > F[k] - 1){
            k++;
        }

        int temp[]= Arrays.copyOf(a, F[k]);         //扩展新的数组，F[k]为新的数组长度，新的数值为默认值0

        for(int i = size; i < F[k] - 1; ++i){
            temp[i] = a[size - 1];
        }

        while(low <= high) {
            int mid = low + F[k - 1] - 1;
            if (target < temp[mid]) {
                high = mid - 1;
                k -= 1;
            } else if (target > temp[mid]) {
                low = mid + 1;
                k -= 2;
            } else {
                if (mid < size)
                    return mid;       //说明mid即为查找到的位置
                else
                    return size - 1; //若mid>=size则说明是扩展的数值,返回size-1
            }
        }
        return -1;
    }

    //分块查找
    //index代表索引数组，t代表待查找数组，keytype代表要查找的元素，m代表每块大小
    public static int blockSearch(int[] index,int[]t,int key,int m){
        int i = search(index,key);    //search函数返回值为带查找元素在第几块
        if(i >= 0){
            int j = m*i;              //j为第i块的第一个元素下标
            int site = (i + 1) * m;
            for( ; j < site; j++){
                if(t[j] == key)
                    return j;
            }
        }
        return -1;
    }
    public static int search(int[]index,int keytype){
        int i;
        for(i = 0; ; i++){
            if(index[i] >= keytype){
                break;
            }
        }
        return i;
    }


    //哈希查找,链表内数据个数需大于11
    public static <T> int shellSearch(int[] a, int key){
        int m = key % 11;
        while(a[m] != key && m < a.length - 1){
            m++;
        }
        if(a[m] != key){
            return -1;
        }
        else{
            return m;
        }
    }
}
