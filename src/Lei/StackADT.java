package Lei;

public interface StackADT<T> {
    public void push (T element); //没有方法体

    public T pop() throws EmptyCollectionException; //出栈，可以无参数

    public T peek() throws EmptyCollectionException;

    public boolean isEmpty();

    public int size();

    public String toString();  //println(stack)
}
