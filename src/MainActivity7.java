package com.example.test7;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity7 extends AppCompatActivity implements View.OnClickListener {

    private Button b1, b2, b3, b4, b5, b6, b7, b8;
    private TextView t1, t2, t3, t4, t5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);
        b1 = (Button) findViewById(R.id.shunxu);
        b2 = (Button) findViewById(R.id.erfen);
        b3 = (Button) findViewById(R.id.charu);
        b4 = (Button) findViewById(R.id.feibonaqi);
        b5 = (Button) findViewById(R.id.fenkuai);
        b6 = (Button) findViewById(R.id.haxi);
        b7 = (Button) findViewById(R.id.xier1);
        b8 = (Button) findViewById(R.id.xier2);
        t1 = (TextView) findViewById(R.id.array);
        t2 = (TextView) findViewById(R.id.value);
        t3 = (TextView) findViewById(R.id.site);
        t4 = (TextView) findViewById(R.id.positive);
        t5 = (TextView) findViewById(R.id.inverse);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        b6.setOnClickListener(this);
        b7.setOnClickListener(this);
        b8.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int index = 0, value, expect;
        String[] detail;
        String array = t1.getText().toString();
        detail = array.split(" ");
        value = Integer.parseInt(t2.getText().toString());
        expect = Integer.parseInt(t3.getText().toString());
        switch (v.getId()) {
            case R.id.shunxu:
                for (int i = detail.length - 1; i >= 0; i--) {
                    if (Integer.parseInt(detail[i]) == value) {
                        index = i;
                    }
                }
                if (index == expect) {
                    Toast.makeText(MainActivity7.this, "true", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity7.this, "false", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.erfen:
                index = BinarySearch2(detail, value, 0, detail.length - 1);
                if (index == expect) {
                    Toast.makeText(MainActivity7.this, "true", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity7.this, "false", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.charu:
                index = InsertionSearch(detail, value, 0, detail.length - 1);
                if (index == expect) {
                    Toast.makeText(MainActivity7.this, "true", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity7.this, "false", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.feibonaqi:
                index = fibonacciSearching(detail, value, detail.length - 1);
                if (index == expect) {
                    Toast.makeText(MainActivity7.this, "true", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity7.this, "false", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.fenkuai:
                int index1[]={10, 90, 2316};           //此三个数为输入数组中每块的最大值,本例将数值分成3块，每块4个数
                index = blockSearch(index1, detail, value, detail.length / 3);
                if (index == expect) {
                    Toast.makeText(MainActivity7.this, "true", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity7.this, "false", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.haxi:                       //数据个数需大于11,为同时契合分块查找和哈希查找，设置12个数
                index = shellSearch(detail, value);
                if (index == expect) {
                    Toast.makeText(MainActivity7.this, "true", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity7.this, "false", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.xier1:
                String a = shellSort_positive(detail);
                String expect1 = t4.getText().toString();
                if (a.equals(expect1)) {
                    Toast.makeText(MainActivity7.this, "true", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity7.this, "false", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.xier2:
                String b = shellSort_inverse(detail);
                String expect2 = t5.getText().toString();
                if (b.equals(expect2)) {
                    Toast.makeText(MainActivity7.this, "true", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity7.this, "false", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    //二分查找，递归版本
    //需要升序排列，返回-1表示搜索失败
    public static <T> int BinarySearch2(String a[], int value, int low, int high) {
        int mid = low + (high - low) / 2;
        if (Integer.parseInt(a[mid]) == value)
            return mid;
        else if (Integer.parseInt(a[mid]) > value)
            return BinarySearch2(a, value, low, mid - 1);
        else if (Integer.parseInt(a[mid]) < value)
            return BinarySearch2(a, value, mid + 1, high);
        else return -1;
    }

    //插值查找
    //需要升序排列，返回-1表示搜索失败
    public static <T> int InsertionSearch(String a[], int value, int low, int high) {
        int mid = low + (value - Integer.parseInt(a[low])) / (Integer.parseInt(a[high]) - Integer.parseInt(a[low])) * (high - low);
        if(Integer.parseInt(a[mid]) == value)
            return mid;
        else if(Integer.parseInt(a[mid]) > value)
            return InsertionSearch(a, value, low, mid-1);
        else if(Integer.parseInt(a[mid]) < value)
            return InsertionSearch(a, value, mid+1, high);
        else return -1;
    }

    //斐波那契查找
    //需要升序排列，返回0表示搜索失败
    //先建立斐波那契数组,定义其长度
    public static void Fibonacci(int F[]){
        int maxSize = 20;
        F[0] = 0;
        F[1] = 1;
        for (int i = 2; i < maxSize; i++){
            F[i] = F[i - 1] + F[i - 2];
        }
    }

    //斐波那契查找方法
    public static <T> int fibonacciSearching(String a[], int target, int size){
        int low = 0;
        int high = size - 1;
        int maxSize = 20;

        //创建斐波那契数组
        int F[] = new int[maxSize];
        Fibonacci(F);

        //计算n位于斐波那契数列的位置
        int k = 0;
        while (size > F[k] - 1){
            k++;
        }

        String temp[]= Arrays.copyOf(a, F[k]);         //扩展新的数组，F[k]为新的数组长度，新的数值为默认值0

        for(int i = size; i < F[k] - 1; ++i){
            temp[i] = a[size - 1];
        }

        while(low <= high) {
            int mid = low + F[k - 1] - 1;
            if (target < Integer.parseInt(temp[mid])) {
                high = mid - 1;
                k -= 1;
            } else if (target > Integer.parseInt(temp[mid])) {
                low = mid + 1;
                k -= 2;
            } else {
                if (mid < size)
                    return mid;       //说明mid即为查找到的位置
                else
                    return size - 1; //若mid>=size则说明是扩展的数值,返回size-1
            }
        }
        return -1;
    }

    //分块查找
    //index代表索引数组，t代表待查找数组，keytype代表要查找的元素，m代表每块大小
    public static int blockSearch(int[] index, String[] t, int key, int m){
        int i = search(index,key);    //search函数返回值为带查找元素在第几块
        if(i >= 0){
            int j = m*i;              //j为第i块的第一个元素下标
            int site = (i + 1) * m;
            for( ; j < site; j++){
                if(Integer.parseInt(t[j]) == key)
                    return j;
            }
        }
        return -1;
    }
    public static int search(int[]index,int keytype){
        int i;
        for(i = 0; ; i++){
            if(index[i] >= keytype){
                break;
            }
        }
        return i;
    }

    //哈希查找,链表内数据个数需大于11
    public static <T> int shellSearch(String[] a, int key){
        int m = key % 11;
        while(Integer.parseInt(a[m]) != key && m < a.length - 1){
            m++;
        }
        if(Integer.parseInt(a[m]) != key){
            return -1;
        }
        else{
            return m;
        }
    }

    //希尔排序,正序
    public static <T>
    String shellSort_positive(String[] a){
        int temp = a.length / 2;
        int first, last;
        while (temp > 0){
            for (int i = 0; i + temp <= a.length - 1; i++){
                first = i;
                last = i + temp;
                if (Integer.parseInt(a[first]) > Integer.parseInt(a[last])){
                    String temp2 = a[first];
                    a[first] = a[last];
                    a[last] = temp2;
                }
            }
            temp = temp / 2;
        }

        String result = "";
        for (int i = 0; i < a.length;  i++){
            result = result + a[i] + " ";
        }
        return result;
    }

    //逆序
    public static <T>
    String shellSort_inverse(String[] a){
        int temp = a.length / 2;
        int first, last;
        while (temp > 0){
            for (int i = 0; i + temp <= a.length - 1; i++){
                first = i;
                last = i + temp;
                if (Integer.parseInt(a[first]) < Integer.parseInt(a[last])){
                    String temp2 = a[first];
                    a[first] = a[last];
                    a[last] = temp2;
                }
            }
            temp = temp / 2;
        }

        for (int i = 0; i + 1 <= a.length - 1; i++){           //经过测试发现我的分块排序法可能会出错，因此加上此条防止出错
            first = i;
            last = i + 1;
            if (Integer.parseInt(a[first]) < Integer.parseInt(a[last])){
                String temp2 = a[first];
                a[first] = a[last];
                a[last] = temp2;
            }
        }

        String result = "";
        for (int i = 0; i < a.length;  i++){
            result = result + a[i] + " ";                      //注：最后一个数后需加空格
        }
        return result;
    }
}