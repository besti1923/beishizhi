import java.util.*;

public class Graph {
    //设有dot个顶点，顶点序号分别为1、2、3...dot。目前顶点数限定在16个以内。
    static int dot, side;               //dot为顶点数，side为边数
    static int[][] concern = new int[15][15];      //矩阵信息
    static int[] link = new int[30];       //用来存续输入的数据
    static int[] visited = new int[15];     //用来标记已遍历的点
    static Scanner scan = new Scanner(System.in);
    static int origin, weight = 0;         //origin为起点，weight为权重
    static int total2 = 0;             //记录各方法中已访问的顶点数
    static int temp = 9999, temp2 = 0;
    static int visited2[] = new int[15];        //按顺序记录Prim算法中被选中的点的编号
    static int[] dist = new int[15];      //记录最短路径长度
    static int[] pre = new int[15];       //记录入度来自哪个顶点
    static Queue list = new LinkedList();
    static Stack stack = new Stack();
    static Stack stack2 = new Stack();

    public static void main(String[] args) {
        System.out.println("====================================");
        System.out.println("           输入1构造无向图");
        System.out.println("           输入2构造有向图");
        System.out.println("====================================");

        int select;
        do{
            select = scan.nextInt();
            if(select == 1){
                CreateUndigraph();
            }
            if(select == 2){
                CreateDigraph();
            }
        }while (select != 1 && select != 2);     //避免用户输入其他奇奇怪怪的数字

        System.out.println("1、初始化，该图的邻接矩阵为：");
        Adjacency_Matrix();         //生成并输出图的邻接矩阵

        origin = search();         //寻找起点

        System.out.println("2.1、图的遍历：广度优先遍历：");
        System.out.print(origin + " ");            //先输出起点并将其标记，然后进行广度优先遍历
        visited[origin - 1] = 1;
        Breadth_Traversal(origin);

        for(int i = 0; i < dot; i++){               //因广度优先遍历时已经访问过所有点，此处需要重新初始化
            visited[i] = 0;
        }

        System.out.println("\n2.2、图的遍历：深度优先遍历：");
        System.out.print(origin + " ");            //先输出起点并将其标记，然后进行深度优先遍历
        visited[origin - 1] = 1;
        Depth_Traversal(origin);

        if(select == 1){
            System.out.println("\n4、Prim算法构建无向图的最小生成树：");
            for(int i = 0; i < dot; i++){              //因遍历时已经访问过，此处需要重新初始化
                visited[i] = 0;
            }
            for(int i = 0; i < dot; i++){
                for(int j = 0; j < dot; j++){
                    if(concern[i][j] == 1){
                        System.out.print("请输入" + (i+1) + " " + (j+1) + "两顶点之间边的权值：");
                        concern[i][j] = scan.nextInt();
                        concern[j][i] = concern[i][j];             //对称先置零避免重复运算
                    }
                }
            }
            System.out.print("请输入起始顶点编号：");
            int v = scan.nextInt();
            Prim(concern, v);
            System.out.println("最小权值：" + weight);
        }

        if(select == 2){                   //我设计的程序中拓扑和迪杰斯特拉算法会改变邻接矩阵，因此此处分开进行
            System.out.println("\n====================================");
            System.out.println("        输入1完成有向图的拓扑排序");
            System.out.println("     输入2完成有向图的单源最短路径求解");
            System.out.println("====================================");

            do{
                select = scan.nextInt();
            }while (select != 1 && select != 2);     //避免用户输入其他奇奇怪怪的数字

            if(select == 1){
                System.out.println("拓扑排序：");
                total2 = 0;
                for(int i = 0; i < dot; i++){              //因之前的方法已经访问过，此处需要重新初始化
                    visited[i] = 0;
                }
                Topology(concern);
            }

            if(select == 2){
                System.out.println("用迪杰斯特拉算法完成有向图的单源最短路径求解：");
                for(int i = 0; i < dot; i++){                        //因遍历时已经访问过，此处需要重新初始化
                    visited[i] = 0;
                }
                System.out.print("请输入起始顶点编号：");
                int v = scan.nextInt();
                for(int i = 0; i < dot; i++){
                    for(int j = 0; j < dot; j++){
                        if(concern[i][j] == 1){
                            System.out.print("请输入" + (i+1) + " " + (j+1) + "两顶点之间边的权值：");
                            concern[i][j] = scan.nextInt();
                        }
                    }
                }

                for(int i = 0; i < dot; i++){
                    dist[i] = concern[v - 1][i];      //记录起始最短距离，0为自身或无穷大
                    if(concern[v - 1][i] != 0){
                        pre[i] = v;                  //记录起始点所连接的点位
                    }
                }

                for(int i = 0; i < dot; i++){                        //先选出距离最短的点
                    if(dist[i] != 0 && dist[i] < temp){
                        temp = dist[i];
                        temp2 = i;
                    }
                }
                concern[v-1][v-1] = 1;              //标记表示已访问
                total2 = 1;                  //初始化并+1
                Dijkstra(concern, temp2 + 1);

                for(int i = 0; i < dot; i++){
                    if(i+1 != v){
                        int tempp = i;
                        while (pre[tempp] != 0){                 //不断访问其上一个点
                            stack2.push(tempp + 1);
                            tempp = pre[tempp] - 1;
                        }
                        System.out.print(v + "到" + (i+1) + "的最短路径为：" + v + " ");
                        while (!stack2.isEmpty()){
                            System.out.print(stack2.pop() + " ");
                        }
                        System.out.println("长度：" + dist[i]);
                    }
                }
            }
        }
    }

    public static void CreateUndigraph(){
        System.out.print("顶点数：");
        dot = scan.nextInt();
        System.out.print("边数：");
        side = scan.nextInt();
        int j = 0;
        for(int i = 0; i < side; i++){
            System.out.print("请输入第" + (i+1) + "条边相连的两点（中间用空格分开）：");
            link[j] = scan.nextInt();
            link[j + 1] = scan.nextInt();
            j = j + 2;
        }
        for(int i = 0; i < j; i = i + 2){
            concern[link[i] - 1][link[i+1] - 1] = 1;
            concern[link[i+1] - 1][link[i] - 1] = 1;
        }
    }

    public static void CreateDigraph(){
        System.out.print("顶点数：");
        dot = scan.nextInt();
        System.out.print("边数：");
        side = scan.nextInt();
        int j = 0;
        for(int i = 0; i < side; i++){
            System.out.print("请输入第" + (i+1) + "条边首尾相连的两点（先首后尾，中间用空格分开）：");
            link[j] = scan.nextInt();
            link[j + 1] = scan.nextInt();
            j = j + 2;
        }
        for(int i = 0; i < j; i = i + 2){
            concern[link[i] - 1][link[i+1] - 1] = 1;
        }
    }

    public static void Adjacency_Matrix(){
        for(int i = 0; i < dot; i++){
            for(int j = 0; j < dot; j++){
                System.out.print(concern[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int search(){                      //寻找起点
        int origin = 0, temp = 0, temp2 = 0;         //origin记录起点，temp记录最高的出度，令其值为-1防止直接进入if，temp2记录该temp便于下次比较
        for(int i = 0; i < dot; i++){
            for(int j = 0; j < dot; j++){
                if(concern[i][j] == 1){
                    temp++;
                }
            }

            if(temp == temp2){                 //若出度相等则选择入度较小的点为起点
                int temp3 = 0, temp4 = 0;        //temp3、temp4分别记录两者的入度数
                for(int q = 0; q < dot; q++){
                    if(concern[q][origin-1] == 0){
                        temp3++;
                    }
                    if(concern[q][i] == 0){
                        temp4++;
                    }
                }
                if(temp3 < temp4){
                    origin = i + 1;
                    temp2 = temp;
                }
            }

            if(temp > temp2){
                origin = i + 1;
                temp2 = temp;
            }

            temp = 0;
        }
        return origin;
    }

    public static void Breadth_Traversal(int origin){
        for(int i = 0; i < dot; i++){
            if(concern[origin - 1][i] == 1){          //寻找该点的邻接点
                if(visited[i] != 1 ) {                //若该点位被访问过，则输出该点
                    System.out.print((i + 1) + " ");
                    list.add(i + 1);                      //降邻接点加入队列
                    visited[i] = 1;                       //标记邻接点，表示已访问
                }
            }
        }

        while(!list.isEmpty()){
            int temp = (int) list.poll();
            Breadth_Traversal(temp);                  //递归至遍历完毕
        }
    }

    public static void Depth_Traversal(int origin){
        for(int i = 0; i < dot; i++){
            if(concern[origin - 1][i] == 1){          //寻找该点的第一个邻接点
                if(visited[i] != 1 ) {                //若该点未被访问过，则输出该点
                    System.out.print((i + 1) + " ");
                    visited[i] = 1;                       //标记邻接点，表示已访问
                    Depth_Traversal(i + 1);         //递归至遍历完毕
                }
            }
        }
    }

    public static void Topology(int AdjMatrix[][]){
        int i ,j, total = 0, temp2 = 0;             //total记录顶点在矩阵中的行数，temp2记录当前入度为0的顶点的个数
        for(j = 0; j < dot; j++){
            for (i = 0; i < dot; i++){
                if(AdjMatrix[i][j] == 0)
                    total++;
                if(AdjMatrix[i][j] == 1)
                    break;
            }
            if(total == dot && visited[j] != 1){        //total=dot时表示入度为0
                stack.push(j);
                visited[j] = 1;
                temp2++;
            }
            total = 0;
        }

        while (!stack.isEmpty()){
            int temp = (int) stack.pop();    //temp记录该点所指向的点
            for(int k = 0; k < dot; k++){
                AdjMatrix[temp][k] = 0;      //去掉该点时该点的指向全部消失
            }
            System.out.print((temp+1) + " ");
            total2++;
        }

        if(total2 < dot && temp2 != 0){
            Topology(AdjMatrix);
        }if(total2 < dot && temp2 == 0){
            System.out.println("存在环，停止拓扑");
        }
    }

    public static void Prim(int AdjMatrix[][], int v){
        int temp = 9999, temp2 = 0, temp3 = 0;         //temp记录与该点相连的边中最小的权值，temp2记录该点所在列数，temp3记录该点所在行数
        visited2[total2] = v;
        total2++;
        for(int i = 0; i < total2; i++){       //寻找已访问的点中权值最小的邻接边
            for(int k = 0; k < dot; k++){
                if(AdjMatrix[visited2[i] - 1][k] > 0 && AdjMatrix[visited2[i] - 1][k] < temp && visited[k] != 1){
                    temp = AdjMatrix[visited2[i] - 1][k];
                    temp2 = k;
                    temp3 = visited2[i] - 1;
                }
            }
        }
        visited[temp3] = 1;
        visited[temp2] = 1;
        AdjMatrix[temp3][temp2] = -1;        //将已访问的点在矩阵中的权值设为-1，避免重复运算
        AdjMatrix[temp2][temp3] = -1;
        weight += temp;
        if(total2  < dot - 1){
            Prim(AdjMatrix, temp2 + 1);
        }else {
            System.out.println("该最小生成树的图的邻接矩阵为：");
            for(int i = 0; i < dot; i++){    //重新转化成邻接矩阵便于直观观察
                for(int j = 0; j < dot; j++){
                    if(AdjMatrix[i][j] == -1){
                        AdjMatrix[i][j] = 1;
                    }else {
                        AdjMatrix[i][j] = 0;
                    }
                    System.out.print(AdjMatrix[i][j] + " ");
                }
                System.out.println();
            }
        }
    }

    public static void Dijkstra(int AdjMatrix[][], int v){
        int temp = 9999, temp2 = 0, j;                         //temp存权值，temp2存最小权值的边指向的顶点在矩阵中的列数，temp2+1为顶点编号
        AdjMatrix[v-1][v-1] = 1;
        for (j = 0; j < dot; j++){
            if(AdjMatrix[v-1][j] != 0 && j != v-1) {
                if(dist[j] == 0 || dist[j] > dist[v-1] + AdjMatrix[v-1][j]) {
                    dist[j] = dist[v - 1] + AdjMatrix[v - 1][j];
                    pre[j] = v;
                }
            }
        }
        for(int i = 0; i < dot; i++){                          //找出下一个距离最下的点
            if(dist[i] != 0 && dist[i] < temp && AdjMatrix[i][i] != 1){
                temp = dist[i];
                temp2 = i;
            }
        }
        total2++;
        if(total2 < dot) {
            Dijkstra(AdjMatrix, temp2 + 1);
        }
    }
}