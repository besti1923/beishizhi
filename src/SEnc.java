import java.io.*;
import java.security.*;
import javax.crypto.*;
public class SEnc{
    public static void main(String args[]) throws Exception{
        //将加密结果保存在文件Senc.dat中
        FileInputStream f=new FileInputStream("key1.dat");
        ObjectInputStream b=new ObjectInputStream(f);
        Key k=(Key)b.readObject( );
        //从文件"key1.dat"中获取密钥

        Cipher cp=Cipher.getInstance("DESede");
        //创建一个密码器(Cipher类)对象，并指定加密算法"DESede"。
        //和KeyGenerator类一样，Cipher类是一个工厂类，不通过new方法创建对象，而是通过getInstance（ ）获取Cipher对象。

        cp.init(Cipher.ENCRYPT_MODE, k);
        //初始化密码器

        String s="Hello World!";
        byte ptext[]=s.getBytes("UTF8");
        //获取等待加密的明文，getBytes( )方法中必须使用参数"UTF8"指定。

        for(int i=0;i<ptext.length;i++){
            System.out.print(ptext[i]+",");
        }
        System.out.println("");
        //打印明文

        byte ctext[]=cp.doFinal(ptext);
        //执行加密，并返回加密的结果。

        for(int i=0;i<ctext.length;i++){
            System.out.print(ctext[i] +",");
        }
        //打印密文

        FileOutputStream f2=new FileOutputStream("SEnc.dat");
        f2.write(ctext);
        //处理加密结果，将密文存入"SEnc.dat"文件
    }
}