// Server Classes
abstract class Data {
    abstract public void DisplayValue();
}
class Floateger extends Data {
    float value;
    Floateger() {
        value=55.55f;                //浮点数+f
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
class FloatFactory extends Factory {
    public Data CreateDataObject(){
        return new Floateger();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d;
    public static void main(String[] args) {
        d = new Document(new FloatFactory());
        d.DisplayData();
    }
}