import java.util.Scanner;

public class task2 {                              //主函数用于提示和输入
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("If you want to compute the operations of a and b");
        char d;         //定义d作为跳出循环的依据
        do{
            System.out.print("Please enter the of a = ");
            double a = scan.nextDouble();           //输入要计算的数的值
            System.out.print("Please enter the of b = ");
            double b = scan.nextDouble();

            System.out.print("If you want to calculate addition, enter 1.\n" +
                    "If you want to calculate subtraction, enter 2.\n" +
                    "If you want to calculate multiplication, enter 3.\n" +
                    "If you want to calculate division method, enter 4.\n" +
                    "If you want to calculate module, enter 5.\n");
            int c = scan.nextInt();               //选择运算符
            if((b==0&&c==4)||(b==0&&c==5)||( c!=1 && c!=2 && c!=3 && c!=4 && c!=5 )){
                System.out.println("Error!");     //如果除法和模运算中除数为0或运算符不是加减乘除模，则报错
            }

            double resule = calculate(a,b,c);     //子函数运算
            System.out.println("resule = "+resule);

            do {
                System.out.println("Do you want to keep counting? y/n");
                d = scan.next().charAt(0);
            }while(d != 'y' && d != 'n');        //是否继续运算，y继续，n退出
        }while(d == 'y');
    }

    public static double calculate(double a, double b, int c) {   //子函数主要负责计算
        if(c==1){
            return a+b;
        }
        else if(c==2){
            return a-b;
        }
        else if(c==3){
            return a*b;
        }
        else if(c==4){
            return a/b;
        }
        else if(c==5){
            return a%b;
        }
        else {
            return 0.0;
        }
    }
}