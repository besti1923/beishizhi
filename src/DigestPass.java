import java.security.*;
public class DigestPass{
    public static void main(String args[ ]) throws Exception{
        String x=args[0];

        MessageDigest m=MessageDigest.getInstance("MD5");
        //生成对象并指定MD5算法

        m.update(x.getBytes("UTF8"));
        //传入需要计算的字符串

        byte s[ ]=m.digest( );
        //计算消息摘要并以字节类型数组返回

        String result="";
        for (int i=0; i<s.length; i++){
            result+=Integer.toHexString((0x000000ff & s[i]) |
                    0xffffff00).substring(6);
        }
        System.out.println(result);
    }
}