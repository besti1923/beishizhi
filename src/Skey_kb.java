import java.io.*;
import java.security.*;
public class Skey_kb{
    public static void main(String args[]) throws Exception{

        FileInputStream f=new FileInputStream("key1.dat");
        ObjectInputStream b=new ObjectInputStream(f);

        Key k=(Key)b.readObject( );
        //用readObject()读取密钥对象，并传入k中
        //因为readObject()返回的是Object类，所以要强制转换成Key类

        byte[ ] kb=k.getEncoded( );
        //创建一个byte类型的数组kb[]，用Key的getEncoded()方法获取密钥编码格式

        FileOutputStream  f2=new FileOutputStream("keykb1.dat");
        f2.write(kb);
        //保存密钥编码格式到"keykb1.dat"文件中

        for(int i=0;i<kb.length;i++){
            System.out.print(kb[i]+",");
            // 打印密钥编码中的内容
        }
    }
}