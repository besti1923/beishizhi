package cn.edu.besti.cs1923.B2316;

import java.util.Arrays;

public class Sorting2 {
    public static <T>
    String positive(int[] a, int size){
        int i, j, tempp = 0;
        for(i = 0; i < size; i++){
            int temp = a[i];
            for(j = i + 1; j < size; j++){
                if(a[j] < temp){
                    temp = a[j];
                    tempp = j;
                }
            }
            for(j = tempp; j > i; j--){
                a[j] = a[j - 1];
            }
            a[i] = temp;
        }
        return Arrays.toString(a);
    }

    public static <T>
    String inverse(int[] a, int size){
        int i, j, tempp = 0;
        for(i = 0; i < size; i++){
            int temp = a[i];
            tempp = i;
            for(j = i + 1; j < size; j++){
                if(a[j] > temp){
                    temp = a[j];
                    tempp = j;
                }
            }
            for(j = tempp; j > i; j--){
                a[j] = a[j - 1];
            }
            a[i] = temp;
        }
        return Arrays.toString(a);
    }
}


/*
import java.util.Arrays;

public class Sorting2 {
    public static <T>
    String positive(int[] a, int size){
        int i, j, tempp = 0;
        for(i = 0; i < size; i++){
            int temp = a[i];
            for(j = i + 1; j < size; j++){
                if(a[j] < temp){
                    temp = a[j];
                    tempp = j;
                }
            }
            for(j = tempp; j > i; j--){
                a[j] = a[j - 1];
            }
            a[i] = temp;
        }
        return Arrays.toString(a);
    }

    public static <T>
    String inverse(int[] a, int size){
        int i, j, tempp = 0;
        for(i = 0; i < size; i++){
            int temp = a[i];
            tempp = i;
            for(j = i + 1; j < size; j++){
                if(a[j] > temp){
                    temp = a[j];
                    tempp = j;
                }
            }
            for(j = tempp; j > i; j--){
                a[j] = a[j - 1];
            }
            a[i] = temp;
        }
        return Arrays.toString(a);
    }
} */
