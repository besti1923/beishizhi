package Tree;

public class HuffNode implements Comparable {
    private HuffNode left,right;
    private char ch;
    private int n;
    private String code;

    public void setLeft(HuffNode left) {
        this.left = left;
    }

    public void setRight(HuffNode right) {
        this.right = right;
    }

    public void setCh(char ch) {
        this.ch = ch;
    }

    public void setN(int sum) {
        this.n = sum;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public HuffNode getLeft() {
        return left;
    }

    public HuffNode getRight() {
        return right;
    }

    public char getCh() {
        return ch;
    }

    public int getN() {
        return n;
    }

    public String getCode() {
        return code;
    }

    //使用列表排序
    @Override
    public int compareTo(Object o) {
        HuffNode a = (HuffNode)o;
        if(this.n > a.n)
            return 1;              //排在o之前
        else
            return -1;             //排在o之后
    }

    @Override
    public String toString() {
        return getCh() + "出现的次数是" + getN() + ",编码为" + getCode();
    }
}