package Tree;

import java.util.Scanner;

public class Expert {
    private LinkedBinaryTree2<String> tree2;

    public Expert(){
        String e1 = "学长/学姐，你们会给我高分的对吗？(y/n)   ○|￣|_";
        String e2 = "谢谢大佬！   o(￣▽￣)ｄ";
        String e3 = "求求给个机会吧！  ┭┮﹏┭┮   (y/n)";
        String e4 = "┭┮﹏┭┮   求求求给个机会吧！   ┭┮﹏┭┮   (y/n)";
        String e5 = "┭┮﹏┭┮   求求求   ┭┮﹏┭┮   求求求给个机会吧！   ┭┮﹏┭┮       (y/n)";
        String e6 = "┗( T﹏T )┛   请务必再运行一次！";

        LinkedBinaryTree2<String> n1,n2,n3,n4,n5,n6,n7,n8;
        n1 = new LinkedBinaryTree2(e2);
        n2 = new LinkedBinaryTree2(e3);
        n3 = new LinkedBinaryTree2(e4);
        n4 = new LinkedBinaryTree2(e5);
        n5 = new LinkedBinaryTree2(e6);
        n6 = new LinkedBinaryTree2(e5,n1,n5);
        n7 = new LinkedBinaryTree2(e4,n1,n6);
        n8 = new LinkedBinaryTree2(e3,n1,n7);

        tree2 = new LinkedBinaryTree2(e1,n1,n8);
    }

    public void start() throws EmptyCollectionException {
        Scanner scan = new Scanner(System.in);

        System.out.println("学长/学姐辛苦啦！！！");
        while(tree2.size() > 1)
        {
            System.out.println(tree2.getRootElement());
            if(scan.nextLine().equalsIgnoreCase("Y"))
                tree2 = tree2.getLeft();
            else
                tree2 = tree2.getRight();
        }
        System.out.println(tree2.getRootElement());
    }
}