package Tree;

import java.io.*;
import java.util.*;

public class HuffmanTree {
    //字母表
    private static char[] ch = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',' '};
    //用于存储文件中的内容
    private char[] a = new char[100];     //若文件内容较多，可以增大数组容量
    //用于统计各字符出现的次数
    private int[] sum = new int[27];
    private HuffNode root;
    //保存文件的单个编码
    private String[] strings = new String[28];  //此处设置28防止145行抛出ArrayIndexOutOfBoundsException错误
    //哈夫曼树的列表形式
    private LinkedList treelist = new LinkedList<HuffNode>();
    //文件字节数
    static int total;

    //构造哈夫曼树，保存树的根节点
    public LinkedList createTree() {
        count();
        //匹配次数和字符构成节点，并加入树的列表
        for (int i = 0; i < 27; i++) {
            HuffNode node = new HuffNode();
            node.setCh(ch[i]);
            node.setN(sum[i]);
            treelist.add(i, node);
        }
        Collections.sort(treelist);
        while (treelist.size() > 1) {
            //获得两个权值最小的节点
            HuffNode first = (HuffNode) treelist.removeFirst();
            HuffNode second = (HuffNode) treelist.removeFirst();
            //将这两个权值最小的节点构造成父节点
            HuffNode parent = new HuffNode();
            parent.setN(first.getN() + second.getN());
            parent.setLeft(first);
            parent.setRight(second);
            //把父节点添加进列表，并重新排序
            treelist.add(parent);
            Collections.sort(treelist);
        }
        root = (HuffNode) treelist.getFirst();
        return treelist;
    }

    //根据哈夫曼树获得各节点编码
    public void getCode(HuffNode root, String code){       //递归编码
        if(root.getLeft() != null){
            getCode(root.getLeft(),code+"0");
        }
        if(root.getRight() != null){
            getCode(root.getRight(),code+"1");
        }
        if(root.getRight() == null && root.getLeft() == null){
            System.out.println(root.getCh() + "的概率是" + (double)root.getN()/total*100 + "%" + "，编码是" + code);
            root.setCode(code);
        }
    }

    public void read(String path,String name) throws IOException {
        //从文件中读取字节流
        int i = 0;
        File f = new File(path,name);
        Reader reader = new FileReader(f);
        while(reader.ready()){
            a[i++] = (char)reader.read();
        }
        total = i;
        reader.close();
        System.out.print("英文文件内容为：");
        for(int k = 0; k < total; k++){
            System.out.print(a[k]);
        }
        System.out.println();
    }

    public void count(){
        //统计各个字母出现的次数并记录
        for(int k = 0; k < total; k++){
            for (int j = 0; j < ch.length; j++){
                if(a[k] == ch[j])
                    sum[j]++;
            }
        }
    }

    //文件压缩
    public void Encode(String path) throws IOException {
        String result="";
        for(int i = 0; i<27; i++){
            result += ch[i] + "" + sum[i] + ",";
        }
        String content = "";
        for(int i = 0; i < total; i++){
            for(int k = 0; k < ch.length; k++){
                if(a[i] == ch[k])
                    content += search(root,ch[k]).getCode() + " ";
            }
        }
        result += content;
        File f = new File(path);
        if(!f.exists()){
            f.createNewFile();
        }
        System.out.println("编码后的文件为：" + content);
        Writer writer=new FileWriter(f);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(result);
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    public HuffNode search(HuffNode root, char c) {
        if(root.getCh() == c){
            return root;
        }
        if(root.getLeft() != null || root.getRight() != null) {
            HuffNode a = search(root.getLeft(),c);
            HuffNode b = search(root.getRight(),c);
            if(a != null)
                return a;
            if(b != null)
                return b;
        }
        return null;
    }

    public HuffNode getRoot() {
        return root;
    }

    public void read2(String path,String name) throws IOException {
        //读取文件
        File file = new File(path,name);
        Reader reader = new FileReader(file);
        BufferedReader bufferedReader=new BufferedReader((new InputStreamReader(new FileInputStream(file),"GBK")));
        String str = "";
        String temp = "";
        while((temp = bufferedReader.readLine()) != null){
            System.out.println("解码前文件内容：" + temp);
            str = temp;
        }
        //获取每个字符的频数(使用逗号分割),以及文件压缩后的内容
        StringTokenizer s = new StringTokenizer(str,",");
        int i = 0;
        while (s.hasMoreTokens()){
            strings[i++] = s.nextToken();
        }
    }

    public LinkedList createTree2(){
        for(int i = 0; i < 27; i++){
            HuffNode temp = new HuffNode();
            temp.setCh(strings[i].charAt(0));
            temp.setN(strings[i].charAt(1) - '0');
            treelist.add(temp);
        }
        Collections.sort(treelist);
        while (treelist.size() > 1) {
            //获得两个权值最小的节点
            HuffNode first = (HuffNode) treelist.removeFirst();
            HuffNode second = (HuffNode) treelist.removeFirst();
            //将这两个权值最小的节点构造成父节点
            HuffNode parent = new HuffNode();
            parent.setN(first.getN() + second.getN());
            parent.setLeft(first);
            parent.setRight(second);
            //把父节点添加进列表，并重新排序
            treelist.add(parent);
            Collections.sort(treelist);
        }
        root= (HuffNode) treelist.getFirst();
        return treelist;
    }

    public void Decode(String path) throws IOException {
        String t = strings[27];
        String result = "";
        StringTokenizer stringTokenizer = new StringTokenizer(t);
        while(stringTokenizer.hasMoreTokens()){
            String temp = stringTokenizer.nextToken();
            result += search2(root,temp).getCh();
        }
        System.out.println("解码后的文件内容为：" + result);

        File f = new File(path);
        Writer writer = new FileWriter(f);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(result);
        bufferedWriter.flush();
        bufferedWriter.close();
    }
    public HuffNode search2(HuffNode root, String code) {
        if (root.getCode() == null) {
            if (root.getLeft() != null || root.getRight() != null) {
                HuffNode a = search2(root.getLeft(), code);
                HuffNode b = search2(root.getRight(), code);
                if (a != null)
                    return a;
                if (b != null)
                    return b;
            }
            return null;
        }
        else if(root.getCode().equals(code)){
            return root;
        }
        return null;
    }
}