package Tree;

import java.util.ArrayList;

public interface BinaryTree<T>
{
    public T getRootElement() throws EmptyCollectionException;
    public BinaryTree<T> getLeft() throws EmptyCollectionException;
    public BinaryTree<T> getRight() throws EmptyCollectionException;
    public boolean contains(T target);
    public T find(T target) throws ElementNotFoundException;
    public boolean isEmpty();
    public int size();
    public String toString();
    public ArrayList<T> preorder();
    public ArrayList<T> inorder();
    public ArrayList<T> postorder();
    public ArrayList<T> levelorder() throws EmptyCollectionException;
}

