package Tree;

import java.util.NoSuchElementException;
import java.util.Stack;
import java.util.StringTokenizer;

public class PoseCalculate {

    static Stack<String> stack = new Stack<>();

    public static double Calculate(String PoseString){                  //计算后缀表达式的+-*/
        String temp = "";
        double result = 0, temp1, temp2;
        StringTokenizer st = new StringTokenizer(PoseString, " ", false);
        do{
            try {
                temp = st.nextToken();
            }catch (NoSuchElementException e){
                break;
            }
            stack.push(temp);
            if(temp.equals("+")){
                stack.pop();
                temp1 = Double.parseDouble(stack.pop());
                temp2 = Double.parseDouble(stack.pop());
                result = temp2 + temp1;
                stack.push(String.valueOf(result));
            }
            if(temp.equals("-")){
                stack.pop();
                temp1 = Double.parseDouble(stack.pop());
                temp2 = Double.parseDouble(stack.pop());
                result = temp2 - temp1;
                stack.push(String.valueOf(result));
            }
            if(temp.equals("*")){
                stack.pop();
                temp1 = Double.parseDouble(stack.pop());
                temp2 = Double.parseDouble(stack.pop());
                result = temp2 * temp1;
                stack.push(String.valueOf(result));
            }
            if(temp.equals("/")){
                stack.pop();
                temp1 = Double.parseDouble(stack.pop());
                temp2 = Double.parseDouble(stack.pop());
                result = temp2 / temp1;
                stack.push(String.valueOf(result));
            }
        }while (stack != null);

        return result;
    }
}
