package Tree;

import java.util.Stack;

public class InToPose {
    
    public static String transform(String InString) {          //将中缀表达式转化成后缀表达式
        Stack<Character> stack = new Stack<Character>();
        String PoseString = "";
        for (int i = 0; i < InString.length(); i++) {
            Character temp;
            char c = InString.charAt(i);
            switch (c) {
                // 忽略空格
                case ' ':
                    break;
                // 碰到'('，push到栈
                case '(':
                    stack.push(c);
                    break;
                // 碰到'+''-'，将栈中所有运算符弹出，送到输出队列中
                case '+':
                case '-':
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == '(') {
                            stack.push('(');
                            break;
                        }
                        PoseString += " " + temp;
                    }
                    stack.push(c);
                    PoseString += " ";
                    break;
                // 碰到'*''/'，将栈中所有乘除运算符弹出，送到输出队列中
                case '*':
                case '/':
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == '(' || temp == '+' || temp == '-') {
                            stack.push(temp);
                            break;
                        } else {
                            PoseString += " " + temp;
                        }
                    }
                    stack.push(c);
                    PoseString += " ";
                    break;
                // 碰到右括号，将靠近栈顶的第一个左括号上面的运算符全部依次弹出，送至输出队列后，再丢弃左括号
                case ')':
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == '(')
                            break;
                        else
                            PoseString += " " + temp;
                    }
                    // PoseString += " ";
                    break;
                //如果是数字，直接送至输出序列
                default:
                    PoseString += c;
            }
        }
        
        //如果栈不为空，把剩余的运算符依次弹出，送至输出序列。
        while (stack.size() != 0) {
            PoseString += " " + stack.pop();
        }
        return PoseString;
    }
}
