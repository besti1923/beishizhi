package Tree;

import java.util.LinkedList;

public class CreateTree {
    TreeNode root = new TreeNode();
    static int i = 0;
    static LinkedList<TreeNode> list = new LinkedList<>();

    public static TreeNode add(char[] ch) {
        TreeNode node = null;
        if(ch[i] == '#'){
            node = null;
            i++;
        }
        else {
            node = new TreeNode();
            node.e = ch[i];
            i++;
            node.left = add(ch);
            node.right = add(ch);
        }
        return node;
    }

    public static void DLR(TreeNode node) {
        if (node != null) {
            System.out.print(node.print() + " ");
            DLR(node.left);
            DLR(node.right);
        }
    }

    public static void LDR(TreeNode node) {
        if(node == null)
            return;
        else {
            LDR(node.left);
            System.out.print(node.print() + " ");
            LDR(node.right);
        }
    }

    public static void LRD(TreeNode node) {
        if(node == null)
            return;
        else {
            LRD(node.left);
            LRD(node.right);
            System.out.print(node.print() + " ");
        }
    }

    static public void level(TreeNode root){
        if(root == null) {
            return;
        }
        list.add(root);
        while (!list.isEmpty()) {
            TreeNode temp = list.poll();                   //检索并删除此列表的头部
            System.out.print(temp.e + " ");
            if(temp.left != null)
                list.add(temp.left);
            if(temp.right != null)
                list.add(temp.right);
        }
    }
}