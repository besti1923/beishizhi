package Tree;

public class TreeNode {
    public char e;
    TreeNode left, right;

    public TreeNode(char e) {
        this.e = e;
        left = right =null;
    }

    public TreeNode(){
        this.left = null;
        this.right = null;
    }

    public char print() {
        return e;
    }
}