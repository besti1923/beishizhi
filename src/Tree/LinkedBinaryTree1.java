package Tree;

import java.util.ArrayList;

public class LinkedBinaryTree1<T> implements BinaryTree<T> {
    protected BTNode<T> root;

    public LinkedBinaryTree1() {
        root = null;
    }

    public LinkedBinaryTree1(T element) {
        root = new BTNode<T>(element);
    }

    public LinkedBinaryTree1(T element, LinkedBinaryTree1<T> left, LinkedBinaryTree1<T> right) {
        root = new BTNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    public T getRootElement() throws EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException ("Get root operation "
                    + "failed. The tree is empty.");

        return root.getElement();
    }

    public LinkedBinaryTree1<T> getLeft() throws EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException ("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree1<T> result = new LinkedBinaryTree1<T>();
        result.root = root.getLeft();

        return result;
    }

    public T find (T target) throws ElementNotFoundException {
        BTNode<T> node = null;

        if (root != null)
            node = root.find(target);

        if (node == null)
            throw new ElementNotFoundException("Find operation failed. "
                    + "No such element in tree.");

        return node.getElement();
    }

    //返回大小
    public int size() {
        int result = 0;

        if (root != null)
            result = root.count();

        return result;
    }

    public LinkedBinaryTree1<T> getRight() throws EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException ("Get right operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree1<T> result = new LinkedBinaryTree1<T>();
        result.root = root.getRight();

        return result;
    }

    public boolean contains (T target) {
        if (root.find(target) == null){
            return false;
        }
        else {
            return true;
        }
    }

    public boolean isEmpty() {
        if (root==null){
            return true;
        }
        else {
            return false;
        }
    }

    //先序
    public ArrayList<T> preorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.preorder (iter);

        return iter;
    }

    //中序
    public ArrayList<T> inorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if(root != null)
            root.inorder (iter);

        return iter;
    }

    //后序
    public ArrayList<T> postorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.postorder (iter);

        return iter;
    }

    //层序
    public ArrayList<T> levelorder() throws EmptyCollectionException {
        LinkedQueue<BTNode<T>> queue = new LinkedQueue<BTNode<T>>();
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
        {
            queue.enqueue(root);
            while (!queue.isEmpty())
            {
                BTNode<T> current = queue.dequeue();

                iter.add (current.getElement());

                if (current.getLeft() != null)
                    queue.enqueue(current.getLeft());
                if (current.getRight() != null)
                    queue.enqueue(current.getRight());
            }
        }
        return iter;
    }

    public String toString() {
        return super.toString();
    }
}
