import java.io.*;
import javax.crypto.*;
public class Skey_DES{
//在当前目录下将生成文件key1.dat，其中包含的密钥可以用于使用Triple-DES算法的加密和解密，将密钥通过对象序列化方式保存在文件中
    public static void main(String args[]) throws Exception{
        KeyGenerator kg=KeyGenerator.getInstance("DESede");  //获取密钥生成器，指定使用"DESede"算法
    //与其它类的对象的创建方法不同，KeyGenerator通过它的静态方法getInstance()来创建对象

        kg.init(168);    //初始化密钥生成器kg，DESede为112或168位，其中112位有效

        SecretKey k=kg.generateKey( );      //生成密钥。
        //Keygenerator类中的generateKey方法可以生成密钥，类型是SecretKey，可用于后面的加密解密

        //通过对象序列化方式将密钥保存在文件中
        FileOutputStream f=new FileOutputStream("key1.dat");
        //创建一个FileOutputStream类f，同时创建并让f指向一个新文件"key1.dat"

        ObjectOutputStream b=new ObjectOutputStream(f);
        //创建一个新的ObjectOutputStream类的对象

        b.writeObject(k);
        //将生成的密钥k序列化后保存在"key1.dat"文件中
    }
}