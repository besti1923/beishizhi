public class Complex{
    double RealPart;
    double ImagePart;

    //定义构造函数,定义复数的实部变量a和虚部变量b
    public Complex(double a,double b){
        this.RealPart = a;
        this.ImagePart = b;
    }

    //覆盖对象,判断boolean
    public String toString(){
        return "RealPart:" + RealPart + ", ImagePart:" + ImagePart;
    }
    public boolean equals(Object z1){
        if (z1 == this){
            return true;
        }
        else{
            return false;
        }
    }

    //定义加减乘除子函数
    public Complex ComplexAdd(Complex z2){
        Complex z3 = new Complex(this.RealPart + z2.RealPart,this.ImagePart + z2.ImagePart);
        return z3;
    }
    public Complex ComplexSub(Complex z2){
        Complex z3 = new Complex(this.RealPart - z2.RealPart,this.ImagePart - z2.ImagePart);
        return z3;
    }
    public Complex ComplexMulti(Complex z2){
        Complex z3 = new Complex(this.RealPart * z2.RealPart - this.ImagePart * z2.ImagePart,
                this.ImagePart * z2.RealPart + this.RealPart * z2.ImagePart);
        return z3;
    }
    public Complex ComplexDiv(Complex z2){
        double t = z2.RealPart * z2.RealPart + z2.ImagePart * z2.ImagePart;
        Complex z3 = new Complex((this.RealPart * z2.RealPart + this.ImagePart * z2.ImagePart) / t,
                (this.ImagePart * z2.RealPart - this.RealPart * z2.ImagePart) / t);
        return z3;
    }
}