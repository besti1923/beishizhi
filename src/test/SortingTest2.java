package test;

import cn.edu.besti.cs1923.B2316.Sorting2;
import junit.framework.TestCase;

public class SortingTest2 extends TestCase {                 //t1—t5为正序检测，t6-t10为逆序检测
    String t1 = "[1, 2, 5, 6, 2316]", t2 = "[1, 23, 2316]", t3 = "[2, 6, 2316]",
            t4 = "[1, 52, 2316, 9999]", t5 = "[2, 3, 6, 2316]", t6 = "[2316, 25, 16, 8]",
            t7 = "[2316, 9, 8]", t8 = "[2316, 10, 5, 4]", t9 = "[9999, 2316, 22, 6]", t10 = "[2316, 55, 9, 8]";

    public void test1(){
        int[] t = {2316,1,5,6,2};
        assertEquals(t1, Sorting2.positive(t, t.length));
    }

    public void test2(){
        int[] t = {1,2316,23};
        assertEquals(t2, Sorting2.positive(t, t.length));
    }

    public void test3(){
        int[] t = {2,6,2316};
        assertEquals(t3, Sorting2.positive(t, t.length));
    }

    public void test4(){
        int[] t = {52,1,9999,2316};
        assertEquals(t4, Sorting2.positive(t, t.length));
    }

    public void test5(){
        int[] t = {2,2316,6,3};
        assertEquals(t5, Sorting2.positive(t, t.length));
    }

    public void test6(){
        int[] t = {25,16,2316,8};
        assertEquals(t6, Sorting2.inverse(t, t.length));
    }

    public void test7(){
        int[] t = {9,2316,8};
        assertEquals(t7, Sorting2.inverse(t, t.length));
    }

    public void test8(){
        int[] t = {2316,10,5,4};
        assertEquals(t8, Sorting2.inverse(t, t.length));
    }

    public void test9(){
        int[] t = {22,2316,9999,6};
        assertEquals(t9, Sorting2.inverse(t, t.length));
    }

    public void test10(){
        int[] t = {2316,55,9,8};
        assertEquals(t10, Sorting2.inverse(t, t.length));
    }
}


/*public class SortingTest2 {                 //t1—t5为正序检测，t6-t10为逆序检测

    public static void main(String[] args) {
        int[] t1 = {2316, 1, 5, 6, 2}, t2 = {1, 2316, 23}, t3 = {2, 6, 2316},
                t4 = {52, 1, 9999, 2316}, t5 = {2, 2316, 6, 3}, t6 = {25, 16, 2316, 8}, t7 = {9, 2316, 8},
                t8 = {2316, 10, 5, 4}, t9 = {22, 2316, 9999, 6}, t10 = {2316, 55, 9, 8};
        System.out.println(Sorting2.positive(t1, t1.length));
        System.out.println(Sorting2.positive(t2, t2.length));
        System.out.println(Sorting2.positive(t3, t3.length));
        System.out.println(Sorting2.positive(t4, t4.length));
        System.out.println(Sorting2.positive(t5, t5.length));
        System.out.println(Sorting2.inverse(t6, t6.length));
        System.out.println(Sorting2.inverse(t7, t7.length));
        System.out.println(Sorting2.inverse(t8, t8.length));
        System.out.println(Sorting2.inverse(t9, t9.length));
        System.out.println(Sorting2.inverse(t10, t10.length));
    }
}
               */

