public class task2Test {             //单元测试
    public static void main(String[] args) {
        double resule = task2.calculate(8,5,1);   //给a，b赋值代入测试，c为运算符号
        if(resule == 13){
            System.out.println("pass1.");                  //加法测试通过
        }
        else{
            System.out.println("fault1.");                //加法测试失败
        }
        resule = task2.calculate(8,5,2);
        if(resule == 3){
            System.out.println("pass2.");                //减法测试通过
        }
        else{
            System.out.println("fault2.");
        }
        resule = task2.calculate(8,5,3);
        if(resule == 40){
            System.out.println("pass3.");               //乘法测试通过
        }
        else{
            System.out.println("fault3.");
        }
        resule = task2.calculate(10,5,4);
        if(resule == 2){
            System.out.println("pass4.");               //除法测试通过
        }
        else{
            System.out.println("fault4.");
        }
        resule = task2.calculate(8,5,5);
        if(resule == 3){
            System.out.println("pass5.");               //模测试通过
        }
        else{
            System.out.println("fault5.");
        }
    }
}