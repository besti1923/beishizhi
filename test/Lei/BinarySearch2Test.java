package Lei;

import junit.framework.TestCase;

public class BinarySearch2Test extends TestCase {
    public void test_BinarySearch2(){
        int[] t = {1, 16, 19, 20, 2316};
        assertEquals(4, BinarySearch2.BinarySearch2(t, 2316, 0, 4));
        assertEquals(0, BinarySearch2.BinarySearch2(t, 1, 0, 4));
        assertEquals(1, BinarySearch2.BinarySearch2(t, 16, 0, 4));
    }
}