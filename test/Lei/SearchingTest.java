package Lei;

import junit.framework.TestCase;
import org.junit.Test;

public class SearchingTest extends TestCase {
    @Test
    public void test1(){                                                            //由于设置了哨兵，所以边界index为1
        String[] t1 = {" ", "1", "2", "2316", "3", "4"};
        assertEquals(true, Searching.linearSearch(t1, "2"));         //正常
        assertEquals(false, Searching.linearSearch(t1, "0"));        //异常
        assertEquals(true, Searching.linearSearch(t1, "1"));         //边界
    }

    @Test
    public void test2(){
        String[] t1 = {"", "nice", "16", "2316", "20", "19"};
        assertEquals(true, Searching.linearSearch(t1, "2316"));      //正常
        assertEquals(false, Searching.linearSearch(t1, "111"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "nice"));      //边界
    }

    @Test
    public void test3(){
        String[] t1 = {"", "2316", "2323", "2019", "456", "654"};
        assertEquals(true, Searching.linearSearch(t1, "2019"));      //正常
        assertEquals(false, Searching.linearSearch(t1, "222"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "2316"));      //边界
    }

    @Test
    public void test4(){
        String[] t1 = {"", "11", "22", "33", "44", "2316"};
        assertEquals(true, Searching.linearSearch(t1, "2316"));      //正常
        assertEquals(false, Searching.linearSearch(t1, "333"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "11"));        //边界
    }

    @Test
    public void test5(){
        String[] t1 = {"", "9", "8", "7", "2316", "6"};
        assertEquals(true, Searching.linearSearch(t1, "7"));         //正常
        assertEquals(false, Searching.linearSearch(t1, "444"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "9"));         //边界
    }
    @Test
    public void test6(){
        String[] t1 = {"", "44", "789", "26", "2316", "321"};
        assertEquals(true, Searching.linearSearch(t1, "26"));        //正常
        assertEquals(false, Searching.linearSearch(t1, "555"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "44"));        //边界
    }
    @Test
    public void test7(){
        String[] t1 = {"", "23", "66", "2316", "22", "9"};
        assertEquals(true, Searching.linearSearch(t1, "66"));        //正常
        assertEquals(false, Searching.linearSearch(t1, "666"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "23"));        //边界
    }
    @Test
    public void test8(){
        String[] t1 = {"", "2323", "16", "2316", "20", "19"};
        assertEquals(true, Searching.linearSearch(t1, "2316"));      //正常
        assertEquals(false, Searching.linearSearch(t1, "777"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "2323"));      //边界
    }
    @Test
    public void test9(){
        String[] t1 = {"", "566", "665", "1", "2", "2316"};
        assertEquals(true, Searching.linearSearch(t1, "2"));         //正常
        assertEquals(false, Searching.linearSearch(t1, "888"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "566"));       //边界
    }

    @Test
    public void test10(){
        String[] t1 = {"", "33", "3", "abc", "0", "2316"};
        assertEquals(true, Searching.linearSearch(t1, "2316"));      //正常
        assertEquals(false, Searching.linearSearch(t1, "999"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "33"));        //边界
    }
}