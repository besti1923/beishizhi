package Lei;

import junit.framework.TestCase;
import org.junit.Test;

public class SortingTest extends TestCase {                 //t1—t5为正序检测，t6-t10为逆序检测
    String t1 = "[1, 2, 5, 6, 2316]", t2 = "[1, 23, 2316]", t3 = "[2, 6, 2316]",
            t4 = "[1, 52, 2316, 9999]", t5 = "[2, 3, 6, 2316]", t6 = "[2316, 25, 16, 8]",
            t7 = "[2316, 9, 8]", t8 = "[2316, 10, 5, 4]", t9 = "[9999, 2316, 22, 6]", t10 = "[2316, 55, 9, 8]";

    @Test
    public void test1(){
        int[] t = {2316,1,5,6,2};
        assertEquals(t1, Sorting.positive(t, t.length));
    }

    @Test
    public void test2(){
        int[] t = {1,2316,23};
        assertEquals(t2, Sorting.positive(t, t.length));
    }

    @Test
    public void test3(){
        int[] t = {2,6,2316};
        assertEquals(t3, Sorting.positive(t, t.length));
    }

    @Test
    public void test4(){
        int[] t = {52,1,9999,2316};
        assertEquals(t4, Sorting.positive(t, t.length));
    }

    @Test
    public void test5(){
        int[] t = {2,2316,6,3};
        assertEquals(t5, Sorting.positive(t, t.length));
    }

    @Test
    public void test6(){
        int[] t = {25,16,2316,8};
        assertEquals(t6, Sorting.inverse(t, t.length));
    }

    @Test
    public void test7(){
        int[] t = {9,2316,8};
        assertEquals(t7, Sorting.inverse(t, t.length));
    }

    @Test
    public void test8(){
        int[] t = {2316,10,5,4};
        assertEquals(t8, Sorting.inverse(t, t.length));
    }

    @Test
    public void test9(){
        int[] t = {22,2316,9999,6};
        assertEquals(t9, Sorting.inverse(t, t.length));
    }

    @Test
    public void test10(){
        int[] t = {2316,55,9,8};
        assertEquals(t10, Sorting.inverse(t, t.length));
    }
}
