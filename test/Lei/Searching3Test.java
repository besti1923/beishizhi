package Lei;

import junit.framework.TestCase;

public class Searching3Test extends TestCase {
    public void test_linearSearch(){                                                //由于设置了哨兵，所以边界index为1
        String[] t = {" ", "1", "2", "2316", "3", "4"};
        assertEquals(true, Searching3.linearSearch(t, "2"));         //正常
        assertEquals(false, Searching3.linearSearch(t, "0"));        //异常
        assertEquals(true, Searching3.linearSearch(t, "1"));         //边界
    }

    public void test_BinarySearch2(){
        int[] t = {1, 16, 19, 20, 2316};
        assertEquals(4, Searching3.BinarySearch2(t, 2316, 0, 4));
    }

    public void test_InsertionSearch(){
        int[] t = {1, 16, 19, 20, 2316};
        assertEquals(1, Searching3.InsertionSearch(t, 16, 0, 4));
    }

    public void test_Fibonacci(){
        int[] t = {1, 16, 19, 20, 2316};
        assertEquals(1, Searching3.fibonacciSearching(t, 16, t.length));
    }

    public void test_shellSearch(){
        int[] t = {1, 2, 3, 4, 2316, 11, 99, 16, 19, 20, 23};
        assertEquals(6, Searching3.shellSearch(t, 99));              //正常
        assertEquals(-1, Searching3.shellSearch(t, 9999));           //异常
    }

    public void test_blockSearch(){
        int index[]={22,48,86};
        int[] t = {22, 12, 13, 8, 9, 20,  33, 42, 44, 38, 24, 48,  60, 58, 74, 49, 86, 53};
        assertEquals(15, Searching3.blockSearch(index, t, 49, 6));       //正常
        assertEquals(11, Searching3.blockSearch(index, t, 48, 6));       //边界
    }
}