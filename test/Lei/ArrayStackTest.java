package Lei;

import java.util.Scanner;

public class ArrayStackTest {
    public static void main(String[] args) throws EmptyCollectionException {
        ArrayStack stack = new ArrayStack();
        String str = "";
        Scanner in=new Scanner(System.in);

        do{
            System.out.println("Enter:");
            str = in.nextLine();
            stack.push(str);                           //push测试
            System.out.println("continue?(y or n):");
            str = in.nextLine();
        }while(str.equalsIgnoreCase("y"));    //输入y则继续入栈

        while(!stack.isEmpty()){                   //循环将栈清空
            System.out.println("Pop:"+stack.pop());           //pop测试
            System.out.println("Isempty:"+stack.isEmpty());     //isEmpty测试
            System.out.println("The peak element is:"+stack.peek());   //peek测试
            System.out.println("Size of the stack:"+stack.size());   //size测试
            System.out.println("Whole elements of the stack:"+stack.toString());
            System.out.println("++++++++++++++++++++++++++++++");
        }
    }
}