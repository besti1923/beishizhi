package Tree;

import junit.framework.TestCase;

public class LinkedBinaryTree2Test extends TestCase {

    LinkedBinaryTree2 t = new LinkedBinaryTree2();
    BTNode tree;

    public void testTree(){
        char[] pre = {'A','B','D','H','I','E','J','M','N','C','F','G','K','L'};
        char[] in = {'H','D','I','B','E','M','J','N','A','F','C','K','G','L'};
        tree = t.construct(pre,in);
        assertEquals("H I D M N J E B F K L G C A ", t.poseOrder(tree));
    }
}