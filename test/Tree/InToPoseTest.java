package Tree;

import junit.framework.TestCase;
import java.util.Scanner;

public class InToPoseTest extends TestCase {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("请输入中缀表达式(各数字和字符间用空格分开):");
        String s = scan.nextLine();
        String s1 = InToPose.transform(s);
        System.out.println("后缀表达式为:" + s1);
        System.out.println("计算结果为:" + PoseCalculate.Calculate(s1));
    }
}