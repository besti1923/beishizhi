package Tree;

import junit.framework.TestCase;

public class LinkedBinaryTree1Test extends TestCase {
    LinkedBinaryTree1 a = new LinkedBinaryTree1(1);
    LinkedBinaryTree1 b = new LinkedBinaryTree1(2);
    LinkedBinaryTree1 c = new LinkedBinaryTree1(3, a, b);
    LinkedBinaryTree1 d = new LinkedBinaryTree1(4);
    LinkedBinaryTree1 e = new LinkedBinaryTree1(2316, c, d);
    LinkedBinaryTree1 f = new LinkedBinaryTree1();

    public void testGetRight() throws EmptyCollectionException {
        System.out.println(e.getRight().toString());
    }

    public void testContains() {
        assertEquals(true, e.contains(2316));
        assertEquals(false, a.contains(6));
    }

    public void testToString() {
        System.out.println(e.toString());
    }

    public void testPreorder() {
        assertEquals("[2316, 3, 1, 2, 4]",e.preorder().toString());
    }

    public void testLevelOrder() throws EmptyCollectionException {
        assertEquals("[2316, 3, 4, 1, 2]",e.levelorder().toString());
    }

    public void testGetRootElement() throws EmptyCollectionException {
        assertEquals(2316, e.getRootElement());
    }

    public void testPostorder() {
        assertEquals("[1, 2, 3, 4, 2316]",e.postorder().toString());
    }

    public void testSize() {
        assertEquals(3, c.size());
        assertEquals(5, e.size());
    }

    public void testIsEmpty() {
        assertEquals(false,a.isEmpty());
        assertEquals(true,f.isEmpty());
    }

    public void testFind() throws ElementNotFoundException {
        assertEquals(2316 ,e.find(2316));
    }
}