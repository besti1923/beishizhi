import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex z1 = new Complex(4.0,2.0);
    Complex z2 = new Complex(1.0,-1.0);
    @Test
    public void testequals() {
        assertEquals(true, z1.equals(z1));
        assertEquals(false, z1.equals(z2));
    }
    @Test
    public void testComplexAdd() {
        assertEquals("RealPart:5.0, ImagePart:1.0", z1.ComplexAdd(z2).toString());
    }
    @Test
    public void testComplexSub() {
        assertEquals("RealPart:3.0, ImagePart:3.0", z1.ComplexSub(z2).toString());
    }
    @Test
    public void testComplexMulti() {
        assertEquals("RealPart:6.0, ImagePart:-2.0", z1.ComplexMulti(z2).toString());
    }
    @Test
    public void testComplexDiv() {
        assertEquals("RealPart:1.0, ImagePart:3.0", z1.ComplexDiv(z2).toString());
    }
}